from tests.test_utils import test_array_to_matrix, test_prepare_matrix_for_device, \
    test_matrix_to_array, test_array_to_matrix_to_array, test_multi_num_matrix_to_scalar, \
    test_num_matrix_to_pixel_matrix, test_multi_matrix_to_scalar, tests_union_matrixes, test_get_transition_screen
from tests.test_event_emitter import test_emitter_on, test_emitter_off, test_emitter_emit
from tests.test_file_storage import test_file_read, test_file_write
from tests.test_button import test_button_down, test_button_up, test_button_press, \
    test_button_calls_all_events_on_press, test_button_long_press, test_button_looong_press

from tests.test_utils import tests_union_matrixes

try:
    import uasyncio
    environment = 'board'
except Exception as e:
    import asyncio as uasyncio
    environment = 'dev'


async def main():
    try:
        await uasyncio.gather(
            test_array_to_matrix(),
            test_prepare_matrix_for_device(),
            test_matrix_to_array(),
            test_array_to_matrix_to_array(),
            test_num_matrix_to_pixel_matrix(),
            test_multi_num_matrix_to_scalar(),
            test_multi_matrix_to_scalar(),
            tests_union_matrixes(),
            test_get_transition_screen(),
            # test_write_number_on_display(),
        )


        await uasyncio.gather(
            test_emitter_on(),
            test_emitter_off(),
            test_emitter_emit(),
        )


        if environment == 'dev':
            await uasyncio.gather(
                test_file_read(),
                test_file_write(),
            )


        await uasyncio.gather(
            test_button_down(),
            test_button_up(),
            test_button_press(),
            test_button_long_press(),
            test_button_looong_press(),
            test_button_calls_all_events_on_press(),
        )

    except Exception as e:
        print(e)
        raise e

    print('============== tests passed ==============')


uasyncio.run(main())
