try:
    import asyncio as uasyncio
    import network
except ImportError:
    import asyncio as uasyncio
    import app.stubs.network as network

from app.events import event_emitter
import json


def extract_form_data(received_data: str) -> dict:
    received_data_list = received_data.split('\r\n\r\n')
    form_data_raw = ''
    if len(received_data_list) == 2 and received_data_list[1]:
        form_data_raw = received_data_list[1]
    else:
        return {}

    form_data = {}
    
    for line in form_data_raw.split('&'):
        (_key, _value) = line.split('=')
        form_data.update({_key: unquote(_value).decode()})

    return form_data

_hextobyte_cache = None


def unquote(string):
    """unquote('abc%20def') -> b'abc def'."""
    global _hextobyte_cache

    # Note: strings are encoded as UTF-8. This is only an issue if it contains
    # unescaped non-ASCII characters, which URIs should not.
    if not string:
        return b''

    if isinstance(string, str):
        string = string.encode('utf-8')

    bits = string.split(b'%')
    if len(bits) == 1:
        return string

    res = [bits[0]]
    append = res.append

    # Build cache for hex to char mapping on-the-fly only for codes
    # that are actually used
    if _hextobyte_cache is None:
        _hextobyte_cache = {}

    for item in bits[1:]:
        try:
            code = item[:2]
            char = _hextobyte_cache.get(code)
            if char is None:
                char = _hextobyte_cache[code] = bytes([int(code, 16)])
            append(char)
            append(item[2:])
        except KeyError:
            append(b'%')
            append(item)

    return b''.join(res)


class Server:
    ssid = 'clocky' 
    password = '12345678' 
    PORT = 2359              # Arbitrary non-privileged port
    HOST = ''                 # Symbolic name meaning all available interfaces
    
    def __init__(self, initial_data):
        self.initial_data = initial_data


    # if you do not see the network you may have to power cycle
    # unplug your pico w for 10 seconds and plug it in again
    async def ap_mode(self, ssid, password):
        """
            Description: This is a function to activate AP mode

            Parameters:

            ssid[str]: The name of your internet connection
            password[str]: Password for your internet connection

            Returns: Nada
        """
        # Just making our internet connection
        self.ap = network.WLAN(network.AP_IF)
        self.ap.active(False)
        self.ap.active(True)

        self.ap.config(essid=ssid, password=password)
        self.ap.active(True)

        while self.ap.active() == False:
            print('waiting for connection creation...')
            await uasyncio.sleep(1)
        
        print('AP Mode Is Active, You can Now Connect')
        print('IP Address To Connect to:: ' + self.ap.ifconfig()[0])
        self.HOST = self.ap.ifconfig()[0]

        return self.ap


    def get_index_html(self):
        with open('app/static/index.html', 'r') as index_html:
            line = index_html.readline()

            yield line

            while line:
                line = index_html.readline()

                yield line

    
    async def handle_echo(self, reader, writer):
        received_data = ''

        received_data = await reader.read(2000)
        received_data = received_data.decode()

        form_data = extract_form_data(received_data)

        if form_data:
            self.initial_data = form_data
            event_emitter.emit('server::post_data', form_data)

        writer.write("HTTP/1.1 200 OK\nContent-Type: text/html; charset=utf-8\n\n".encode())

        for line in self.get_index_html():
            writer.write(line.replace('{{}}', json.dumps(self.initial_data)).encode())

        await writer.drain()

        print("Close the connection")
        writer.close()
        await writer.wait_closed()


    async def create_server(self):
        await self.ap_mode(self.ssid, self.password)
        self.server = await uasyncio.start_server(self.handle_echo, self.HOST, self.PORT)

        while True:
            await uasyncio.sleep(1)


    def destroy_server(self):
        if hasattr(self, 'ap') and self.ap.active() != False: 
            self.ap.active(False)

        if hasattr(self, 'server'): 
            self.server.close()
