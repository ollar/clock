from app.stubs.tkscreen import App

try:
    from ulab import numpy as np
    import uasyncio
except ImportError:
    import numpy as np
    import asyncio as uasyncio


class StateMachine:
    def __init__(self, screen_buffer, *args, **kwargs) -> None:
        self.screen = App()
        self.screen_buffer = screen_buffer


    def active(self, mode):
        self.mode = mode

    
    def put(self, value, shift=0):
        self.screen.write(value)


async def data_transfer_bus(screen_buffer):
    state_machine = StateMachine(screen_buffer)

    while True:
        state_machine.put(screen_buffer.buffer)
        await uasyncio.sleep(0.1)
