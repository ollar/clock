import tkinter as tk
from app.events import event_emitter


LED_COLS = 16
LED_ROWS = 16
LED_SIZE = 20
LED_GAP = 10

LEDS_NUMBER = LED_COLS * LED_ROWS


def rgb_to_hex(rgb):
    return ('#%02x%02x%02x' % rgb)


class App():
    def __init__(self):
        self.window = Window()


    def write(self, _matrix):
        self.window.draw(_matrix)


class Window():
    def __init__(self):
        self.root = tk.Tk()
        self.root.resizable(width=False, height=False)
        self.root.geometry(f'{LED_COLS * LED_SIZE + (LED_COLS + 1) * LED_GAP}x{LED_ROWS * LED_SIZE + (LED_ROWS + 1) * LED_GAP}')

        self.init_canvas()

        self.root.bind('<KeyPress-1>', self.handle_button_down)
        self.root.bind('<KeyRelease-1>', self.handle_button_up)
        self.root.bind('<KeyPress-2>', self.handle_button_down)
        self.root.bind('<KeyRelease-2>', self.handle_button_up)
        self.root.bind('<KeyPress-3>', self.handle_button_down)
        self.root.bind('<KeyRelease-3>', self.handle_button_up)

        self.root.bind('<Key-4>', self.handle_button_press)
        self.root.bind('<Key-5>', self.handle_button_press)
        self.root.bind('<Key-6>', self.handle_button_press)

        self.root.bind('<Key-7>', self.handle_button_long_press)
        self.root.bind('<Key-8>', self.handle_button_long_press)
        self.root.bind('<Key-9>', self.handle_button_long_press)

        self.root.bind('<Key-0>', self.handle_button_looong_press)

        self.root.bind('<Key-Left>', self.handle_encoder_change)
        self.root.bind('<Key-Right>', self.handle_encoder_change)

        # down


    def handle_button_down(self, event):
        print(f'button::{int(event.keysym)-1}::down')

        event_emitter.emit(f'button::{int(event.keysym)-1}::down')


    def handle_button_up(self, event):
        print(f'button::{int(event.keysym)-1}::up')

        event_emitter.emit(f'button::{int(event.keysym)-1}::up')


    def handle_button_press(self, event):
        print(f'button::{int(event.keysym)-4}::press')

        event_emitter.emit(f'button::{int(event.keysym)-4}::press')

    
    def handle_button_long_press(self, event):
        print(f'button::{int(event.keysym)-7}::long_press')
        
        event_emitter.emit(f'button::{int(event.keysym)-7}::long_press')

    
    def handle_button_looong_press(self, event):
        print(f'button::{int(event.keysym)}::looong_press')

        event_emitter.emit(f'button::{int(event.keysym)}::looong_press')


    def handle_encoder_change(self, event):
        if event.keysym == 'Right':
            return event_emitter.emit(f'encoder::increased')

        if event.keysym == 'Left':
            return event_emitter.emit(f'encoder::decreased')


    def init_canvas(self):
        main_frame = tk.Frame(self.root)
        main_frame.place(relwidth=1, relheight=1)
        self.root.update()

        self.canvas = tk.Canvas(main_frame, width=main_frame.winfo_width(), height=main_frame.winfo_height(), bg='darkBlue')
        self.canvas.pack()

        for row in range(0, LED_ROWS):
            for col in range(0, LED_COLS):
                self.canvas.create_rectangle(
                    LED_GAP + row*LED_SIZE + row*LED_GAP,
                    LED_GAP + col*LED_SIZE + col*LED_GAP,
                    LED_GAP + row*LED_SIZE + row*LED_GAP + LED_SIZE,
                    LED_GAP + col*LED_SIZE + col*LED_GAP + LED_SIZE,
                    fill="#e3e",
                    tags=(f'{row}::{col}')
                )


    def draw(self, matrix):
        for row_index, row in enumerate(matrix):
            for col_index, col in enumerate(row):
                try:
                    self.canvas.itemconfig((f'{col_index}::{row_index}'), fill=rgb_to_hex(col))
                except Exception as e:
                    raise e

        self.root.update()

