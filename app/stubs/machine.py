try:
    import datetime
    import uasyncio
except ImportError:
    import asyncio as uasyncio


class Pin():
    IN = 'IN'
    PULL_UP = 'PULL_UP'

    def __init__(self, pin, *args):
        self.pin = pin
        self._value = 0 

    def value(self, _value = None):
        if _value is not None:
            self._value = _value

        return self._value


class TouchPad():
    def __init__(self, pin, *args):
        self.pin = pin
        self._value = 0


    def read(self):
        return 500


class RTC():
    def __init__(self):
        self.dd = datetime.datetime.now(datetime.UTC)
        self.loop = uasyncio.get_event_loop()
        self.loop.create_task(self.tick())


    async def tick(self):
        await uasyncio.sleep(1)
        dd = self.dd + datetime.timedelta(0,1)
        self.dd = dd
        return await self.tick()


    def set_time(self, hours=0, minutes=0, seconds=0):
        dd = datetime.datetime(self.dd.year, self.dd.month, self.dd.day, hours, minutes, seconds)
        self.dd = dd
        return dd


    def get_time(self):
        tt = self.dd.timetuple()
        (tm_year, tm_mon, tm_mday, tm_hour, tm_min, tm_sec, tm_wday, tm_yday, tm_isdst, ) = tt
        return (tm_year, tm_mon, tm_mday, 0, tm_hour, tm_min, tm_sec, 0)


    def datetime(self, time_tuple = None):
        if (time_tuple):
            (tm_year, tm_mon, tm_mday, _, tm_hour, tm_min, tm_seconds, *args) = time_tuple
            return self.set_time(tm_hour, tm_min, tm_seconds)

        return self.get_time()
