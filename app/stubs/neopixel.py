from app.stubs.tkscreen import App

try:
    from ulab import numpy as np
except ImportError:
    import numpy as np


class NeoPixel(dict):
    _matrix = []

    def __init__(self, PIN, NUM_LEDS):
        self._matrix = np.zeros(NUM_LEDS, dtype=(int, 3))
        self.screen = App()


    def __setitem__(self, position, value):
        # self._matrix[position] = tuple(map(lambda x: x*10, value))
        print(position, value)
        self._matrix[position] = value


    def write(self):
        self.screen.write(self._matrix)
