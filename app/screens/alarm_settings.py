from app.alarm import Alarm
from app.screens.clock_settings import ClockSettingsScreen


class AlarmSettingsScreen(ClockSettingsScreen):
    ACTIVE_COLOR = (250,255,0)
    PALE_COLOR = (150,150,0)

    def __init__(self, matrix, clock):
        self.alarm = Alarm()

        super().__init__(matrix, clock)

        (hours, minutes) = self.alarm.get_time()

        self.settings_time = {
            "hours": hours,
            "minutes": minutes,
        }


    def destroy(self):
        self.alarm.set_time(**self.settings_time)

        self.remove_event_listeners()
        print('del')
