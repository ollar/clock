from lib.functools import partial
from app.screens.base import Screen
from app.helpers import chars
from app.utils import multi_tuple_matrix_to_scalar
from app.events import event_emitter
from app.alarm import Alarm


def rjust(string, width, char):
    if len(string) < width:
        return char * (width - len(string)) + string
    return string


class ClockScreen(Screen):
    def __init__(self, buffer, clock):
        self.clock = clock
        self.alarm = Alarm()
        super().__init__(buffer)


    @property
    def color(self) -> tuple:
        return (250,250,250)


    @property
    def time(self):
        (_, _, _, _, hours, minutes, seconds, _) = self.clock.get_time()
        return hours, minutes, seconds


    @property
    def time_matrixes(self):
        hours, minutes, _  = self.time
        hours, minutes = str(hours), str(minutes)
        hours, minutes = rjust(hours, 2,'0'), rjust(minutes, 2,'0')

        hours = [chars[num] for num in [h for h in hours]]
        minutes = [chars[num] for num in [m for m in minutes]]

        return hours, minutes


    def render_hours(self):
        hours, _ = self.time_matrixes
        hours = [multi_tuple_matrix_to_scalar(hour, self.color) for hour in hours]

        # add hour numbers
        self.screen_buffer.update(hours[0], (6,2))
        self.screen_buffer.update(hours[1], (10,2))


    def render_minutes(self):
        _, minutes = self.time_matrixes

        minutes = [multi_tuple_matrix_to_scalar(minute, self.color) for minute in minutes]

        #add minute numbers
        self.screen_buffer.update(minutes[0], (6,9))
        self.screen_buffer.update(minutes[1], (10,9))


    def render_seconds(self):
        _, _, seconds = self.time

        self.screen_buffer.update([[self.color]], (seconds % 4, seconds % 3))
        self.screen_buffer.update([[self.color]], (seconds % 2 + 3, seconds % 2 + 7))


    def render_time(self):
        self.render_hours()
        self.render_minutes()
        self.render_seconds()


    def render(self):
        clock_hours, clock_minutes, _ = self.time
        alarm_hours, alarm_minutes = self.alarm.get_time()

        self.screen_buffer.clear()

        if int(clock_hours) == int(alarm_hours) and int(clock_minutes) == int(alarm_minutes):
            event_emitter.emit('init::alarm_screen')

        self.render_time()


    def add_event_listeners(self):
        print('add_event_listeners clock')
        event_emitter.on('button::0::down', partial(event_emitter.emit, 'init::clock_settings_screen'))
        event_emitter.on('button::2::down', partial(event_emitter.emit, 'init::alarm_settings_screen'))
        event_emitter.on('encoder::increased', partial(self.screen_buffer.increase_brightness))
        event_emitter.on('encoder::decreased', partial(self.screen_buffer.decrease_brightness))


    def remove_event_listeners(self):
        print('remove_event_listeners clock')
        event_emitter.off('button::0::down')
        event_emitter.off('button::2::down')
        event_emitter.off('encoder::increased')
        event_emitter.off('encoder::decreased')
