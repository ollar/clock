from app.screens.base import Screen
from app.events import event_emitter


class ZenScreen(Screen):
    _sleep_time = 0.1

    colors = [
        (255,0,0),
        (0,255,0),
        (0,0,255),
        (255,255,0),
        (255,0,255),
        (0,255,255),
        (255,255,255),
    ]

    active_color_1 = 0
    active_color_2 = 1

    iteration = 1


    @property
    def color_1(self):
        return self.colors[self.active_color_1]


    @property
    def color_2(self):
        return self.colors[self.active_color_2]


    def toggle_color_1_is_selecting(self, *args):
        self.color_1_is_selecting = not self.color_1_is_selecting


    def toggle_color_2_is_selecting(self, *args):
        self.color_2_is_selecting = not self.color_2_is_selecting


    def roll_active_color_next(self, *args):
        self.colors = self.colors[1:] + self.colors[:1]


    def roll_active_color_back(self, *args):
        self.colors = self.colors[-1:] + self.colors[:-1]


    def add_event_listeners(self):
        print('add_event_listeners zen')
        event_emitter.on('encoder::increased', self.roll_active_color_next)
        event_emitter.on('encoder::decreased', self.roll_active_color_back)


    def remove_event_listeners(self):
        print('remove_event_listeners zen')
        event_emitter.off('encoder::increased')
        event_emitter.off('encoder::decreased')


    def render(self):
        self.screen_buffer.clear()

        # self.screen_buffer.noise_martix_two_colors(self.color_1, self.color_2, self.iteration)
        self.screen_buffer.noise_martix_two_colors2_mixed(self.color_1, self.color_2, self.iteration)

        self.iteration = self.iteration + 1
