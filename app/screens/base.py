class Screen():
    _sleep_time = 0.1

    def __init__(self, buffer):
        self.screen_buffer = buffer

        self.add_event_listeners()

    
    def __call__(self) -> None:
        return

    
    def add_event_listeners(self) -> None:
        raise Exception('this method should be overriden')


    def remove_event_listeners(self) -> None:
        raise Exception('this method should be overriden')


    def render(self) -> None:
        raise Exception('this method should be overriden')


    def destroy(self) -> None:
        self.remove_event_listeners()
        print('del')
