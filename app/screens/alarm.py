from lib.functools import partial
from app.screens.clock import ClockScreen
from app.events import event_emitter


class AlarmScreen(ClockScreen):
    MAX_ALARM_LIFE = 3000

    def __init__(self, matrix, clock):
        self.clock = clock
        self.iteration = 0
        super().__init__(matrix, clock)


    @property
    def _color(self):
        alarm_power = self.iteration / 6000
        return (min(255,255*alarm_power*10), min(255,255 * alarm_power), 0)


    def render_alarm_screen(self):
        self.screen_buffer.noise_martix_one_color2(self._color, self.iteration)


    def add_event_listeners(self):
        print('add_event_listeners alarm')
        event_emitter.on('button::0::down', partial(event_emitter.emit, 'init::clock_screen'))
        event_emitter.on('button::1::down', partial(event_emitter.emit, 'init::clock_screen'))
        event_emitter.on('button::2::down', partial(event_emitter.emit, 'init::clock_screen'))


    def remove_event_listeners(self):
        print('remove_event_listeners alarm')
        event_emitter.off('button::0::down')
        event_emitter.off('button::1::down')
        event_emitter.off('button::2::down')


    def render(self):
        self.iteration = self.iteration + 1

        if self.iteration >= self.MAX_ALARM_LIFE:
            event_emitter.emit('init::clock_screen')

        self.screen_buffer.clear()
        self.render_alarm_screen()
        self.render_time()
