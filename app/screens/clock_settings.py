from lib.functools import partial
from app.screens.clock import ClockScreen
from app.utils import multi_tuple_matrix_to_scalar
from app.events import event_emitter


class ClockSettingsScreen(ClockScreen):
    CONTROLS = ['hours', 'minutes']
    ACTIVE_CONTROL = 'hours'
    CONTROLS_MAX_VALUES = {
        'hours': 24,
        'minutes': 60,
    }
    ACTIVE_COLOR = (250,0,0)
    PALE_COLOR = (150,0,0)


    def __init__(self, buffer, clock):
        super().__init__(buffer, clock)

        (_, _, _, _, hours, minutes, seconds, _) = clock.get_time()

        self.settings_time = {
            "hours": hours,
            "minutes": minutes,
            "seconds": seconds,
        }


    @property
    def seconds(self):
        (_, _, _, _, _, _, seconds, _) = self.clock.get_time()

        return seconds


    @property
    def time(self):
        return self.settings_time['hours'], self.settings_time['minutes'], self.seconds


    @property
    def color(self):
        return self.ACTIVE_COLOR


    @property
    def active_control(self):
        return self.ACTIVE_CONTROL


    def render_hours(self):
        hours, _ = self.time_matrixes

        color = self.color
        if self.active_control == 'hours':
            color = self.ACTIVE_COLOR if self.seconds % 2 == 0 else self.PALE_COLOR
        hours = [multi_tuple_matrix_to_scalar(hour, color) for hour in hours]

         # add hour numbers
        self.screen_buffer.update(hours[0], (6,2))
        self.screen_buffer.update(hours[1], (10,2))


    def render_minutes(self):
        _, minutes = self.time_matrixes

        color = self.color
        if self.active_control == 'minutes':
            color = self.ACTIVE_COLOR if self.seconds % 2 == 0 else self.PALE_COLOR
        minutes = [multi_tuple_matrix_to_scalar(minute, color) for minute in minutes]

        #add minute numbers
        self.screen_buffer.update(minutes[0], (6,9))
        self.screen_buffer.update(minutes[1], (10,9))


    def render(self):
        self.screen_buffer.clear()
        self.render_time()


    def toggle_settings_active_control(self, *args):
        self.CONTROLS = self.CONTROLS[1:] + self.CONTROLS[:1]
        self.ACTIVE_CONTROL = self.CONTROLS[0]


    def increase(self, *args):
        hours, minutes, _ = self.time

        new_value = (self.settings_time[self.active_control] + 1) % self.CONTROLS_MAX_VALUES[self.active_control]
        self.settings_time.update({self.active_control: new_value})

        print(self.settings_time)


    def decrease(self, *args):
        hours, minutes, _ = self.time

        new_value = (self.settings_time[self.active_control] - 1) % self.CONTROLS_MAX_VALUES[self.active_control]
        self.settings_time.update({self.active_control: new_value})

        print(self.settings_time)


    def add_event_listeners(self):
        print('add_event_listeners clock_settings')
        event_emitter.on('button::0::down', partial(event_emitter.emit, 'init::clock_screen'))
        event_emitter.on('button::2::down', self.toggle_settings_active_control)
        event_emitter.on('encoder::increased', self.increase)
        event_emitter.on('encoder::decreased', self.decrease)


    def remove_event_listeners(self):
        print('remove_event_listeners clock_settings')
        event_emitter.off('button::0::down')
        event_emitter.off('button::2::down')
        event_emitter.off('encoder::increased')
        event_emitter.off('encoder::decreased')


    def destroy(self):
        self.clock.set_time(**self.settings_time)

        super().destroy()
