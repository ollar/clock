from lib.functools import partial
from app.screens.clock import ClockScreen
from app.events import event_emitter


class SleepScreen(ClockScreen):
    def __init__(self, matrix, clock):
        self.clock = clock
        self.iteration = 1
        self.max_iterations = 24000
        self.half_iterations = self.max_iterations // 2
        super().__init__(matrix, clock)


    @property
    def _color(self):
        red = (max(self.max_iterations - self.iteration, 0) / self.max_iterations) * 255
        green = (max(self.half_iterations - self.iteration, 0) / self.half_iterations) * 255

        return (red, green, 0)


    def add_event_listeners(self):
        print('add_event_listeners sleep')
        event_emitter.on('encoder::increased', partial(self.screen_buffer.increase_brightness))
        event_emitter.on('encoder::decreased', partial(self.screen_buffer.decrease_brightness))

        event_emitter.emit('utils::time_sync')


    def remove_event_listeners(self):
        print('remove_event_listeners sleep')
        event_emitter.off('encoder::increased')
        event_emitter.off('encoder::decreased')


    def render(self):
        self.screen_buffer.clear()
        self.screen_buffer.noise_martix_one_color2(self._color, self.iteration)

        if self.iteration == self.max_iterations:
            event_emitter.emit('button::0::press')

        if self.iteration >= self.half_iterations:
            self.render_time()

        self.iteration = self.iteration + 1


    def destroy(self):
        self.remove_event_listeners()
        print('del sleep')
