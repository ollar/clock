from lib.functools import partial
from app.screens.base import Screen
from app.helpers import perlin_screen_matrix_waves
from app.server import Server
from app.events import event_emitter
from app.file import FileStorage
from app.helpers import chars
from app.utils import multi_tuple_matrix_to_scalar


try:
    import uasyncio
except Exception as e:
    import asyncio as uasyncio


class ConfigScreen(Screen):
    _sleep_time = 0.5
    _file_name = 'configs'

    colors = [(255,255,0), (155,155,0)]

    def __init__(self, matrix, clock):
        self.clock = clock
        self.config_file_storage = FileStorage(filename=self._file_name)
        self.server = Server(self.config_file_storage.read())
        self.shift_x = 0

        super().__init__(matrix)


    def __call__(self) -> None:
        uasyncio.create_task(self.server.create_server())
        

    def add_event_listeners(self):
        print('add_event_listeners configs')
        event_emitter.on('server::post_data', self.handle_server_post_data)
        event_emitter.on('encoder::increased', partial(self.screen_buffer.increase_brightness))
        event_emitter.on('encoder::decreased', partial(self.screen_buffer.decrease_brightness))


    def remove_event_listeners(self):
        print('remove_event_listeners configs')
        event_emitter.off('server::post_data')
        event_emitter.off('encoder::increased')
        event_emitter.off('encoder::decreased')


    def destroy(self):
        super().destroy()
        self.server.destroy_server()


    def render_address(self):
        if not self.server.HOST:
            return

        position_x = 0
        position_y = 6

        host = self.server.HOST
        port = self.server.PORT

        address = f'{host}:{port}'

        for i in range(self.shift_x, len(address)):
            number = chars[address[i]]
            number = multi_tuple_matrix_to_scalar(number, self.colors[i % 2])

            self.screen_buffer.update(number, (position_x, position_y))

            position_x = position_x + len(number[0])

        self.shift_x += 1

        if self.shift_x >= len(address):
            self.shift_x = 0

        print(address, len(address), self.shift_x)


    def render(self):
        self.screen_buffer.clear()

        self.render_address()


    def handle_server_post_data(self, data):
        _data = self.config_file_storage.read()
        self.clock.set_time(hours=int(data.get('time_hours')), minutes=int(data.get('time_minutes')))

        del data['time_hours']
        del data['time_minutes']

        _data.update({k: v for k, v in data.items() if v})

        self.config_file_storage.write(_data)
