from app.events import event_emitter

try:
    import rotary_irq_rp2
    import uasyncio
except ImportError:
    from app.stubs import rotary_irq_rp2
    import asyncio as uasyncio


class Rotary:
    def __init__(self, pin_num_clk, pin_num_dt, name='encoder'):
        self.name = name
        self.encoder = rotary_irq_rp2.RotaryIRQ(
            pin_num_clk=pin_num_clk,
            pin_num_dt=pin_num_dt,
            reverse=False,
            pull_up=True
        )
        self.value = self.encoder.value()


    async def listen_changes(self):
        while True:
            new_value = self.encoder.value()

            if self.value != new_value:
                if new_value > self.value:
                    print(f'{self.name}::increased', new_value)
                    event_emitter.emit(f'{self.name}::increased', new_value)
                else:
                    print(f'{self.name}::decreased', new_value)
                    event_emitter.emit(f'{self.name}::decreased', new_value)
                self.value = new_value

            await uasyncio.sleep(0.01)
