try:
    import machine
except ImportError:
    from app.stubs import machine


class Clock():
    def __init__(self):
        self.rtc = machine.RTC()

    def get_time(self):
        return self.rtc.datetime()

    def set_time(self, hours=0, minutes=0, seconds=0):
        print('set_time !!!!', hours, minutes, seconds)
        self.rtc.datetime((2022, 8, 3, 1, hours, minutes, seconds, 0))
        return self.rtc
