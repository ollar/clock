from app.file import FileStorage

try:
    import uasyncio
    import network
except ImportError:
    import asyncio as uasyncio
    import app.stubs.network as network


class TimeSync:
    _config_file_name = 'configs'
    _hostname = 'msu.ru'


    def __init__(self, clock):
        self.clock = clock
        self.file_storage = FileStorage(filename=self._config_file_name)
        self.configs = self.file_storage.read()


    async def enable_wlan(self):
        if self.configs.get('wifi_login') and self.configs.get('wifi_password'):
            # network activate
            self.wlan = network.WLAN(network.STA_IF)
            self.wlan.active(True)
            self.wlan.connect(self.configs.get('wifi_login'), self.configs.get('wifi_password'))

            # Wait for connection to establish
            max_wait = 10
            while not self.wlan.isconnected() and max_wait > 0:
                max_wait -= 1
                print('waiting for connection...')
                await uasyncio.sleep(1)

            # Manage connection errors
            if self.wlan.status() != network.STAT_GOT_IP:
                print('Network Connection has failed')
            else:
                print('connected')
                status = self.wlan.ifconfig()
                print( 'ip = ' + status[0] )


    def disable_wlan(self):
        if hasattr(self, 'wlan'):
            self.wlan.active(False)


    async def make_request(self):
        if hasattr(self, 'wlan') and self.wlan.isconnected():
            reader, writer = await uasyncio.open_connection(self._hostname, 80)

            query = (
                f"HEAD / HTTP/1.0\r\n"
                f"Host: {self._hostname}\r\n"
                f"\r\n"
            )
            writer.write(query.encode())
            while True:
                line = await reader.readline()
                if not line:
                    break

                line = line.decode().rstrip()
                if line and line.startswith('Date'):
                    date = line[-12:]
                    date = date[:8]
                    hours, minutes, seconds, *_ = date.split(':')
                    print(hours, minutes, seconds)
                    if hours and minutes:
                        hours = int(hours) + int(self.configs.get('timezone', '0'))
                        self.clock.set_time(hours=int(hours), minutes=int(minutes), seconds=int(seconds))
                    return

            # Ignore the body, close the socket 
            writer.close()
            await writer.wait_closed()


    async def sync(self):
        await self.enable_wlan()
        await self.make_request()
        self.disable_wlan()
