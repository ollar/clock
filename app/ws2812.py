import uasyncio
import array
import machine


try:
    import rp2

    @rp2.asm_pio(sideset_init=rp2.PIO.OUT_LOW, out_shiftdir=rp2.PIO.SHIFT_LEFT, autopull=True,
    pull_thresh=24)
    def screen_statemachine():
        T1 = 2
        T2 = 5
        T3 = 3
        wrap_target()
        label("bitloop")
        out(x, 1) .side(0) [T3 - 1]
        jmp(not_x, "do_zero") .side(1) [T1 - 1]
        jmp("bitloop") .side(1) [T2 - 1]
        label("do_zero")
        nop() .side(0) [T2 - 1]
        wrap()


    async def data_transfer_bus(screen_buffer):
        state_machine = rp2.StateMachine(0, screen_statemachine, freq=8_000_000, sideset_base=machine.Pin(4))
        state_machine.active(1)

        arr = array.array('I', [0 for i in range(256)])

        while True:
            item = 0
            for row_number, row in enumerate(screen_buffer.buffer):
                if row_number % 2 == 1:
                    for i in range(0, len(row), 4):
                        r = int(row[i][0] * screen_buffer.BRIGHTNESS)
                        g = int(row[i][1] * screen_buffer.BRIGHTNESS)
                        b = int(row[i][2] * screen_buffer.BRIGHTNESS)

                        arr[item] = ((g<<16) + (r<<8) + b)

                        r = int(row[i+1][0] * screen_buffer.BRIGHTNESS)
                        g = int(row[i+1][1] * screen_buffer.BRIGHTNESS)
                        b = int(row[i+1][2] * screen_buffer.BRIGHTNESS)

                        arr[item+1] = ((g<<16) + (r<<8) + b)

                        r = int(row[i+2][0] * screen_buffer.BRIGHTNESS)
                        g = int(row[i+2][1] * screen_buffer.BRIGHTNESS)
                        b = int(row[i+2][2] * screen_buffer.BRIGHTNESS)

                        arr[item+2] = ((g<<16) + (r<<8) + b)

                        r = int(row[i+3][0] * screen_buffer.BRIGHTNESS)
                        g = int(row[i+3][1] * screen_buffer.BRIGHTNESS)
                        b = int(row[i+3][2] * screen_buffer.BRIGHTNESS)

                        arr[item+3] = ((g<<16) + (r<<8) + b)

                        item += 4
                else:
                    for i in range(len(row)-1, 0, -4):
                        r = int(row[i][0] * screen_buffer.BRIGHTNESS)
                        g = int(row[i][1] * screen_buffer.BRIGHTNESS)
                        b = int(row[i][2] * screen_buffer.BRIGHTNESS)

                        arr[item] = ((g<<16) + (r<<8) + b)

                        r = int(row[i-1][0] * screen_buffer.BRIGHTNESS)
                        g = int(row[i-1][1] * screen_buffer.BRIGHTNESS)
                        b = int(row[i-1][2] * screen_buffer.BRIGHTNESS)

                        arr[item+1] = ((g<<16) + (r<<8) + b)

                        r = int(row[i-2][0] * screen_buffer.BRIGHTNESS)
                        g = int(row[i-2][1] * screen_buffer.BRIGHTNESS)
                        b = int(row[i-2][2] * screen_buffer.BRIGHTNESS)

                        arr[item+2] = ((g<<16) + (r<<8) + b)

                        r = int(row[i-3][0] * screen_buffer.BRIGHTNESS)
                        g = int(row[i-3][1] * screen_buffer.BRIGHTNESS)
                        b = int(row[i-3][2] * screen_buffer.BRIGHTNESS)

                        arr[item+3] = ((g<<16) + (r<<8) + b)

                        item += 4

            state_machine.put(arr, 8)

            await uasyncio.sleep(0.1)
except ImportError:
    import neopixel

    async def data_transfer_bus(screen_buffer):
        screen_matrix = neopixel.NeoPixel(machine.Pin(4), 256)

        while True:
            item = 0
            for row_number, row in enumerate(screen_buffer.buffer):
                if row_number % 2 == 1:
                    for i in range(0, len(row), 4):
                        r = int(row[i][0] * screen_buffer.BRIGHTNESS)
                        g = int(row[i][1] * screen_buffer.BRIGHTNESS)
                        b = int(row[i][2] * screen_buffer.BRIGHTNESS)

                        screen_matrix[item] = (r,g,b)

                        r = int(row[i+1][0] * screen_buffer.BRIGHTNESS)
                        g = int(row[i+1][1] * screen_buffer.BRIGHTNESS)
                        b = int(row[i+1][2] * screen_buffer.BRIGHTNESS)

                        screen_matrix[item+1] = (r,g,b)

                        r = int(row[i+2][0] * screen_buffer.BRIGHTNESS)
                        g = int(row[i+2][1] * screen_buffer.BRIGHTNESS)
                        b = int(row[i+2][2] * screen_buffer.BRIGHTNESS)

                        screen_matrix[item+2] = (r,g,b)

                        r = int(row[i+3][0] * screen_buffer.BRIGHTNESS)
                        g = int(row[i+3][1] * screen_buffer.BRIGHTNESS)
                        b = int(row[i+3][2] * screen_buffer.BRIGHTNESS)

                        screen_matrix[item+3] = (r,g,b)

                        item += 4
                else:
                    for i in range(len(row)-1, 0, -4):
                        r = int(row[i][0] * screen_buffer.BRIGHTNESS)
                        g = int(row[i][1] * screen_buffer.BRIGHTNESS)
                        b = int(row[i][2] * screen_buffer.BRIGHTNESS)

                        screen_matrix[item] = (r,g,b)

                        r = int(row[i-1][0] * screen_buffer.BRIGHTNESS)
                        g = int(row[i-1][1] * screen_buffer.BRIGHTNESS)
                        b = int(row[i-1][2] * screen_buffer.BRIGHTNESS)

                        screen_matrix[item+1] = (r,g,b)

                        r = int(row[i-2][0] * screen_buffer.BRIGHTNESS)
                        g = int(row[i-2][1] * screen_buffer.BRIGHTNESS)
                        b = int(row[i-2][2] * screen_buffer.BRIGHTNESS)

                        screen_matrix[item+2] = (r,g,b)

                        r = int(row[i-3][0] * screen_buffer.BRIGHTNESS)
                        g = int(row[i-3][1] * screen_buffer.BRIGHTNESS)
                        b = int(row[i-3][2] * screen_buffer.BRIGHTNESS)

                        screen_matrix[item+3] = (r,g,b)

                        item += 4

            screen_matrix.write()

            await uasyncio.sleep(0.1)
