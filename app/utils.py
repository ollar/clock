from lib.functools import partial
from lib.operator import mul
from lib.itertools import chain


def deepcopy(arr):
    def copy_item(item):
        if type(item) == list:
            return deepcopy(item)
        return item

    return [copy_item(item) for item in arr]


def multi_tuple(t1: tuple, t2: tuple) -> tuple:
    return tuple(map(mul, t1, t2))


def multi_tuple_to_scalar(t1: tuple, k: float) -> tuple:
    return tuple(map(partial(min, 255), map(partial(int), map(partial(mul, k), t1))))


def sum_tuple(t1: tuple, t2: tuple) -> tuple:
    return tuple(map(partial(min, 255), map(sum, zip(t1, t2))))


def divide_tuple(t: tuple, n: int) -> tuple:
    return tuple(map(lambda _t: _t//n, t))


divide_tuple_2 = partial(divide_tuple, n=2)


def get_transition_screen(list1: list, list2: list) -> list:
    return list(map(divide_tuple_2, map(lambda t:sum_tuple(t[0],t[1]), zip(list1, list2))))


def array_to_matrix(array, chunk_size):
    return [array[i:i+chunk_size] for i in range(0, len(array), chunk_size)]


def prepare_matrix_for_device(matrix):
    def reverse_even(enumerable):
        i, item = enumerable
        return item[::-1] if i % 2 == 0 else item

    return map(reverse_even, enumerate(matrix))


def matrix_to_array(matrix):
    return chain(*matrix)


def num_matrix_to_pixel_matrix(matrix):
    def num_to_pixel(x):
        if type(x) == list:
            return num_matrix_to_pixel_matrix(x)
        return (x,x,x)
    return list(map(num_to_pixel, matrix))


def multi_num_matrix_to_scalar(matrix: list, scalar: int) -> list:
    def mul(x):
        if type(x) == list:
            return multi_num_matrix_to_scalar(x, scalar)
        return x * scalar
    return list(map(mul, matrix))


def multi_tuple_matrix_to_scalar(matrix: list, scalar: tuple) -> list:
    def mul(x):
        if type(x) == list:
            return multi_tuple_matrix_to_scalar(x, scalar)
        return multi_tuple(x, scalar)
    return list(map(mul, matrix))


# matrix1 dims > matrix2 dims
def union_matrixes(matrix1, matrix2, shift=(0,0)):
    cols_shift, rows_shift = shift

    for row_number in range(rows_shift, len(matrix1)):
        for col_number in range(cols_shift, len(matrix1[row_number])):
            try:
                matrix1[row_number][col_number] = sum_tuple(
                    matrix1[row_number][col_number],
                    matrix2[row_number-rows_shift][col_number-cols_shift]
                )
            except IndexError:
                pass

