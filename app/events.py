class EventsEmitter():
    def __init__(self):
        self._events = {}


    def on(self, event_name, callback):
        if not self._events.get(event_name):
            self._events[event_name] = []

        self._events[event_name].append(callback)


    def off(self, event_name, callback = None):
        if not self._events.get(event_name):
            return

        if not callback:
            self._events[event_name].clear()
            return

        self._events[event_name].remove(callback)


    def emit(self, event_name, *args, **kwargs):
        if not self._events.get(event_name):
            return

        for fn in self._events[event_name]:
            fn(*args, **kwargs)


event_emitter = EventsEmitter()
