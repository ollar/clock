import json


class FileStorage():
    _fallback = {}


    def __init__(self, filename):
        self.filename = filename


    def read(self) -> dict:
        try:
            with open(self.filename, 'r') as _file:
                data = _file.read()
                return json.loads(data)
        except OSError:
            return self._fallback

    
    def write(self, data: dict):
        with open(self.filename, 'w') as _file:
            _file.write(json.dumps(data))
