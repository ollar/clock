import time
from app.events import event_emitter

try:
    import uasyncio
except ImportError:
    import asyncio as uasyncio


class Button:
    def __init__(self, pin, button_name, is_pull_up=False):
        self.pin = pin
        self.is_pull_up = is_pull_up
        self.value = self._read_value()
        self.button_name = button_name
        self.button_down_ms = None
        self.button_pressed = False
        self.button_long_pressed = False
        self.button_looong_pressed = False


    @property
    def high(self):
        return 0 if self.is_pull_up else 1

    
    @property
    def low(self):
        return 1 if self.is_pull_up else 0


    def _read_value(self):
        return self.pin.value()


    def _from_low_to_high(self, value):
        return self.value == self.low and value == self.high


    def _from_high_to_low(self, value):
        return self.value == self.high and value == self.low


    @property
    def _pressed_and_hold(self):
        return self._read_value() == self.high and self.button_down_ms


    async def _listen_down(self):
        value = self._read_value()
        if self._from_low_to_high(value):
            print(self.button_name, f'button::{self.button_name}::down')
            self.button_down_ms = time.time_ns()
            event_emitter.emit(f'button::{self.button_name}::down')
            self.value = value

    
    async def _listen_up(self):
        value = self._read_value()
        if self._from_high_to_low(value):
            print(self.button_name, f'button::{self.button_name}::up')
            self.button_down_ms = None
            self.button_pressed = False
            self.button_long_pressed = False
            self.button_looong_pressed = False
            event_emitter.emit(f'button::{self.button_name}::up')

            self.value = value


    async def _listen_press(self):
        if self._pressed_and_hold:
            button_down_diff = (time.time_ns() - self.button_down_ms) // 1_000_000

            if button_down_diff > 500 and not self.button_pressed:
                print(self.button_name, f'button::{self.button_name}::press')
                event_emitter.emit(f'button::{self.button_name}::press')
                self.button_pressed = True
            if button_down_diff > 3000 and not self.button_long_pressed:
                print(self.button_name, f'button::{self.button_name}::long_press')
                event_emitter.emit(f'button::{self.button_name}::long_press')
                self.button_long_pressed = True
            if button_down_diff > 5000 and not self.button_looong_pressed:
                print(self.button_name, f'button::{self.button_name}::looong_press')
                event_emitter.emit(f'button::{self.button_name}::looong_press')
                self.button_looong_pressed = True


    async def listen_changes(self):
        while True:
            await self._listen_down()
            await self._listen_press()
            await self._listen_up()

            await uasyncio.sleep(0.01)
