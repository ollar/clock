from app.file import FileStorage


class Alarm():
    _file_name = 'configs'
    _alarm_time = (-1, -1)

    def __init__(self):
        self.file_storage = FileStorage(filename=self._file_name)
        self._alarm_time = self._read_alarm_time()

    def _read_alarm_time(self):
        alarm_time = self.file_storage.read()

        return (alarm_time.get('alarm_hours', -1), alarm_time.get('alarm_minutes', -1)) 


    def _save_alarm_time(self, hours, minutes):
        prev_data = self.file_storage.read()
        data = {}

        data.update(prev_data)
        data.update({"alarm_hours": hours, "alarm_minutes": minutes})

        self.file_storage.write(data)   


    def get_time(self):
        return self._alarm_time


    def set_time(self, hours, minutes):
        self._save_alarm_time(hours, minutes)
        self._alarm_time = hours, minutes

        return hours, minutes
