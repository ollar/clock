from app.button import Button

try:
    import uasyncio
    import machine
except ImportError:
    import asyncio as uasyncio
    from app.stubs import machine


class TouchPad(Button):
    def __init__(self, pin, button_name):
        self.pin = pin
        self.touch = machine.TouchPad(pin)
        self.value = self._read_value()
        self.button_name = button_name
        self.button_down_ms = None
        self.button_pressed = False
        self.button_long_pressed = False
        self.button_looong_pressed = False


    @property
    def threshold(self):
        return 500


    def _read_value(self):
        return self.touch.read()


    def _from_low_to_high(self, value):
        return self.value > self.threshold and value < self.threshold


    def _from_high_to_low(self, value):
        return self.value < self.threshold and value > self.threshold


    @property
    def _pressed_and_hold(self):
        return self._read_value() < self.threshold and self.button_down_ms
