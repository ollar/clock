from app.events import event_emitter
from app.clock import Clock
from lib.functools import partial

from app.rotary import Rotary
from app.button import Button
from app.touch import TouchPad

from app.net_time_sync import TimeSync
from app.screens.alarm_settings import AlarmSettingsScreen
from app.screens.base import Screen
from app.screens.clock import ClockScreen
from app.screens.clock_settings import ClockSettingsScreen
from app.screens.zen import ZenScreen
from app.screens.alarm import AlarmScreen
from app.screens.sleep import SleepScreen
from app.screens.configs import ConfigScreen
from app.screen_buffer import ScreenBuffer

try:
    import machine
    import uasyncio
    from app.ws2812 import data_transfer_bus
except ImportError:
    import asyncio as uasyncio
    from app.stubs import machine, rp2
    from app.stubs.rp2 import data_transfer_bus


class App():
    NUM_LEDS = 256
    SCREEN_PIN_NUM = 4
    SCREEN_CHUNK_SIZE = 16

    current_screen: Screen

    def __init__(self):
        self.screen_buffer = ScreenBuffer(
            num_leds=self.NUM_LEDS,
            screen_chunk_size=self.SCREEN_CHUNK_SIZE,
        )

        self.clock = Clock()
        self.clock.set_time()

        # init encoder
        self.encoder = Rotary(
            pin_num_clk=19,
            pin_num_dt=18,
        )

        self.time_sync = TimeSync(self.clock)

        # init buttons
        # self.buttons = [
        #     Button(machine.Pin(13, machine.Pin.IN, machine.Pin.PULL_UP), '0', is_pull_up=True),
        #     Button(machine.Pin(12, machine.Pin.IN, machine.Pin.PULL_UP), '1', is_pull_up=True),
        #     Button(machine.Pin(14, machine.Pin.IN, machine.Pin.PULL_UP), '2', is_pull_up=True),
        # ]

        self.buttons = [
            TouchPad(machine.Pin(13), '0'),
            TouchPad(machine.Pin(12), '1'),
            TouchPad(machine.Pin(14), '2'),
        ]

        self.current_screen = ClockScreen(self.screen_buffer, self.clock)

        uasyncio.create_task(self.listen_controls())

        self.add_event_listeners()


    async def init_screen(self):
        uasyncio.create_task(data_transfer_bus(self.screen_buffer))

        uasyncio.create_task(self.time_sync.sync())

        while True:
            self.current_screen.render()
            await uasyncio.sleep(self.current_screen._sleep_time)


    def make_time_sync(self):
        try:
            uasyncio.create_task(self.time_sync.sync())
        except Exception as e:
            print('sync error', e)


    def add_event_listeners(self):
        event_emitter.on('init::clock_screen', partial(self.change_active_screen, 'clock'))
        event_emitter.on('init::clock_settings_screen', partial(self.change_active_screen, 'clock_settings'))
        event_emitter.on('init::alarm_screen', partial(self.change_active_screen, 'alarm'))
        event_emitter.on('init::alarm_settings_screen', partial(self.change_active_screen, 'alarm_settings'))
        event_emitter.on('utils::time_sync', partial(self.make_time_sync))


        event_emitter.on('button::0::press', partial(self.change_active_screen, 'clock'))
        event_emitter.on('button::1::press', partial(self.change_active_screen, 'zen'))
        event_emitter.on('button::2::press', partial(self.change_active_screen, 'sleep'))

        event_emitter.on('button::0::looong_press', partial(self.change_active_screen, 'configs'))


    def change_active_screen(self, screen_name):
        self.current_screen.destroy()
        self.screen_buffer.clear()

        if screen_name == 'clock':
            self.current_screen = ClockScreen(self.screen_buffer, self.clock)
        elif screen_name == 'clock_settings':
            self.current_screen = ClockSettingsScreen(self.screen_buffer, self.clock)
        elif screen_name == 'zen':
            self.current_screen = ZenScreen(self.screen_buffer)
        elif screen_name == 'alarm':
            self.current_screen = AlarmScreen(self.screen_buffer, self.clock)
        elif screen_name == 'alarm_settings':
            self.current_screen = AlarmSettingsScreen(self.screen_buffer, self.clock)
        elif screen_name == 'sleep':
            self.current_screen = SleepScreen(self.screen_buffer, self.clock)
        elif screen_name == 'configs':
            self.current_screen = ConfigScreen(self.screen_buffer, self.clock)

        self.current_screen()


    async def listen_controls(self):
        for button in self.buttons:
            uasyncio.create_task(button.listen_changes())
        uasyncio.create_task(self.encoder.listen_changes())
