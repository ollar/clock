from app.helpers import screen_matrix, perlin_screen_matrix, perlin_screen_two_colors_matrix, perlin_screen_matrix_waves
from app.utils import union_matrixes, multi_tuple_to_scalar
import random


class ScreenBuffer:
    BRIGHTNESS = 0.1

    def __init__(self, num_leds, screen_chunk_size):
        self.NUM_LEDS = num_leds
        self.SCREEN_CHUNK_SIZE = screen_chunk_size

        self.buffer = screen_matrix(self.SCREEN_CHUNK_SIZE)

        self.noise_matrix = [random.random() for i in range(self.SCREEN_CHUNK_SIZE * self.SCREEN_CHUNK_SIZE)]
        self.transition_matrix = [0 for i in range(self.SCREEN_CHUNK_SIZE * self.SCREEN_CHUNK_SIZE)]

    
    def set_brightness(self, brightness):
        self.BRIGHTNESS = brightness


    def increase_brightness(self, *args, **kwargs):
        self.BRIGHTNESS = min(self.BRIGHTNESS + 0.01, 0.2)


    def decrease_brightness(self, *args, **kwargs):
        self.BRIGHTNESS = max(self.BRIGHTNESS - 0.01, 0)

    
    def clear(self):
        for row_number in range(len(self.buffer)):
            for col_number in range(len(self.buffer[row_number])):
                self.buffer[row_number][col_number] = (0,0,0)

    
    def noise_martix_one_color(self, pixel: tuple = (0,0,0), seed = 1):
        random_matrix = perlin_screen_matrix(self.SCREEN_CHUNK_SIZE, pixel, seed)

        self.update(list(random_matrix))

        del random_matrix


    def noise_martix_two_colors(self, pixel1: tuple = (0,0,0), pixel2: tuple = (255,255,255), seed = 1):
        random_matrix = perlin_screen_two_colors_matrix(self.SCREEN_CHUNK_SIZE, pixel1, pixel2, seed)

        self.update(list(random_matrix))

        del random_matrix

    def noise_martix_two_colors2_mixed(self, pixel1: tuple = (0,0,0), pixel2: tuple = (255,255,255), seed = 1):
        for row_number in range(len(self.buffer)):
            for col_number in range(len(self.buffer[row_number])):
                random_matrix_position = row_number * len(self.buffer[row_number]) + col_number

                middle_color = self.noise_matrix[random_matrix_position]

                # try:
                #     left = self.buffer[row_number][col_number-1]
                # except IndexError:
                #     left = self.buffer[row_number][col_number]

                try:
                    right = self.buffer[row_number][col_number+1]
                except IndexError:
                    right = self.buffer[row_number][col_number]

                # try:
                #     top = self.buffer[row_number+1][col_number]
                # except IndexError:
                #     top = self.buffer[row_number][col_number]

                try:
                    bottom = self.buffer[row_number-1][col_number]
                except IndexError:
                    bottom = self.buffer[row_number][col_number]

                if middle_color > 0.95:
                    self.transition_matrix[random_matrix_position] = -0.1
                elif middle_color < 0.05:
                    self.transition_matrix[random_matrix_position] = 0.1


                if self.noise_matrix[random_matrix_position] > 0.5:
                    pixel = pixel1
                else:
                    pixel = pixel2

                # mixed_color = map(lambda x: x // 2, map(sum, zip(left, top, right, bottom, pixel)))
                # mixed_color = map(sum, zip(left, top, right, bottom, pixel))
                mixed_color = map(sum, zip(right, bottom, pixel))
                # mixed_color = tuple(mixed_color)

                # print(mixed_color)

                self.buffer[row_number][col_number] = multi_tuple_to_scalar(mixed_color, self.noise_matrix[random_matrix_position])

                self.noise_matrix[random_matrix_position] = max(0, min(1, self.noise_matrix[random_matrix_position] + self.transition_matrix[random_matrix_position]))

        self.transition_matrix.append(self.transition_matrix.pop(0))


    def noise_martix_two_colors2(self, pixel1: tuple = (0,0,0), pixel2: tuple = (255,255,255), seed = 1):
        for row_number in range(len(self.buffer)):
            for col_number in range(len(self.buffer[row_number])):
                random_matrix_position = row_number * len(self.buffer[row_number]) + col_number

                middle_color = self.noise_matrix[random_matrix_position]

                if middle_color > 0.85:
                    self.transition_matrix[random_matrix_position] = -0.1
                elif middle_color < 0.15:
                    self.transition_matrix[random_matrix_position] = 0.1


                if self.noise_matrix[random_matrix_position] > 0.5:
                    pixel = pixel1
                else:
                    pixel = pixel2

                self.buffer[row_number][col_number] = multi_tuple_to_scalar(pixel, self.noise_matrix[random_matrix_position])

                self.noise_matrix[random_matrix_position] = max(0, min(1, self.noise_matrix[random_matrix_position] + self.transition_matrix[random_matrix_position]))

        self.transition_matrix.append(self.transition_matrix.pop(0))


    def noise_martix_one_color2(self, pixel1: tuple = (0,0,0), seed = 1):

        for row_number in range(len(self.buffer)):
            for col_number in range(len(self.buffer[row_number])):
                random_matrix_position = row_number * len(self.buffer[row_number]) + col_number

                middle_color = self.noise_matrix[random_matrix_position]

                if middle_color > 0.85:
                    self.transition_matrix[random_matrix_position] = -0.1
                elif middle_color < 0.15:
                    self.transition_matrix[random_matrix_position] = 0.1

                self.buffer[row_number][col_number] = multi_tuple_to_scalar(pixel1, self.noise_matrix[random_matrix_position])


                # self.noise_matrix[random_matrix_position] = max(0, min(1, self.noise_matrix[random_matrix_position] + self.transition_matrix[random_matrix_position]))
                self.noise_matrix[random_matrix_position] = self.noise_matrix[random_matrix_position] + self.transition_matrix[random_matrix_position]

        self.transition_matrix.append(self.transition_matrix.pop(0))


    def noise_martix_waves(self, pixel: tuple = (250,250,250), seed = 1):
        random_matrix = perlin_screen_matrix_waves(self.SCREEN_CHUNK_SIZE, pixel, seed)

        self.update(list(random_matrix))

        del random_matrix


    def update(self, new_matrix, shifts=(0,0)):
        union_matrixes(self.buffer, new_matrix, shifts)
