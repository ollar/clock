import math
from lib.functools import partial
from app.utils import multi_tuple_to_scalar


def screen_matrix(chunk_size: int, pixel: tuple = (0,0,0)):
    return [[pixel for item in range(chunk_size)] for row in range(chunk_size)]


def noise(x, y, step):
    # return math.tan(x + math.tan(y + math.tan(step))) % 1
    return math.tan(x + math.tan(y + math.tan(step)))


def noise2(x, y, step):
    return math.sin(x + math.sin(y + math.sin(step))) % 1


def noise3(x, y, step):
    return math.sinh(x) + math.sinh(y) - math.sinh(step)


def perlin_screen_matrix(chunk_size: int, pixel: tuple = (0,0,0), seed = 1):
    def create_cols(i, j):
        n = noise(i, j, seed)
        return multi_tuple_to_scalar(pixel, abs(n))

    def create_rows(chunk_size, i):
        return list(map(partial(create_cols, i), range(chunk_size)))

    return map(partial(create_rows, chunk_size), range(chunk_size))


def perlin_screen_two_colors_matrix(chunk_size: int, pixel1: tuple = (0,0,0), pixel2: tuple = (255,255,255), seed = 1):
    def create_cols(i, j):
        n = noise(i, j, seed)
        pixel = pixel1 if n > 0 else pixel2
        return multi_tuple_to_scalar(pixel, abs(n))

    def create_rows(chunk_size, i):
        return list(map(partial(create_cols, i), range(chunk_size)))

    return map(partial(create_rows, chunk_size), range(chunk_size))


def perlin_screen_matrix_waves(chunk_size: int, pixel: tuple = (0,0,0), seed = 1):
    def create_cols(i, j):
        n = noise2(i, j, seed)
        return multi_tuple_to_scalar(pixel, abs(n))

    def create_rows(chunk_size, i):
        return list(map(partial(create_cols, i), range(chunk_size)))

    return map(partial(create_rows, chunk_size), range(chunk_size))

   

zero = [
    [(1,1,1),(1,1,1),(1,1,1)],
    [(1,1,1),(0,0,0),(1,1,1)],
    [(1,1,1),(0,0,0),(1,1,1)],
    [(1,1,1),(0,0,0),(1,1,1)],
    [(1,1,1),(1,1,1),(1,1,1)],
]

one = [
    [(0,0,0),(1,1,1),(1,1,1)],
    [(0,0,0),(0,0,0),(1,1,1)],
    [(0,0,0),(0,0,0),(1,1,1)],
    [(0,0,0),(0,0,0),(1,1,1)],
    [(0,0,0),(0,0,0),(1,1,1)],
]

two = [
    [(1,1,1),(1,1,1),(1,1,1)],
    [(0,0,0),(0,0,0),(1,1,1)],
    [(1,1,1),(1,1,1),(1,1,1)],
    [(1,1,1),(0,0,0),(0,0,0)],
    [(1,1,1),(1,1,1),(1,1,1)],
]

three = [
    [(1,1,1),(1,1,1),(1,1,1)],
    [(0,0,0),(0,0,0),(1,1,1)],
    [(1,1,1),(1,1,1),(1,1,1)],
    [(0,0,0),(0,0,0),(1,1,1)],
    [(1,1,1),(1,1,1),(1,1,1)],
]

four = [
    [(1,1,1),(0,0,0),(1,1,1)],
    [(1,1,1),(0,0,0),(1,1,1)],
    [(1,1,1),(1,1,1),(1,1,1)],
    [(0,0,0),(0,0,0),(1,1,1)],
    [(0,0,0),(0,0,0),(1,1,1)],
]

five = [
    [(1,1,1),(1,1,1),(1,1,1)],
    [(1,1,1),(0,0,0),(0,0,0)],
    [(1,1,1),(1,1,1),(1,1,1)],
    [(0,0,0),(0,0,0),(1,1,1)],
    [(1,1,1),(1,1,1),(1,1,1)],
]

six = [
    [(1,1,1),(1,1,1),(1,1,1)],
    [(1,1,1),(0,0,0),(0,0,0)],
    [(1,1,1),(1,1,1),(1,1,1)],
    [(1,1,1),(0,0,0),(1,1,1)],
    [(1,1,1),(1,1,1),(1,1,1)],
]

seven = [
    [(1,1,1),(1,1,1),(1,1,1)],
    [(0,0,0),(0,0,0),(1,1,1)],
    [(0,0,0),(1,1,1),(1,1,1)],
    [(0,0,0),(0,0,0),(1,1,1)],
    [(0,0,0),(0,0,0),(1,1,1)],
]

eight = [
    [(1,1,1),(1,1,1),(1,1,1)],
    [(1,1,1),(0,0,0),(1,1,1)],
    [(1,1,1),(1,1,1),(1,1,1)],
    [(1,1,1),(0,0,0),(1,1,1)],
    [(1,1,1),(1,1,1),(1,1,1)],
]

nine = [
    [(1,1,1),(1,1,1),(1,1,1)],
    [(1,1,1),(0,0,0),(1,1,1)],
    [(1,1,1),(1,1,1),(1,1,1)],
    [(0,0,0),(0,0,0),(1,1,1)],
    [(1,1,1),(1,1,1),(1,1,1)],
]

colon = [
    [(0,0,0)],
    [(1,1,1)],
    [(0,0,0)],
    [(1,1,1)],
    [(0,0,0)],
]

dot = [[(1,1,1)]]

chars = {
    "0": zero,
    "1": one,
    "2": two,
    "3": three,
    "4": four,
    "5": five,
    "6": six,
    "7": seven,
    "8": eight,
    "9": nine,
    ".": dot,
    ":": colon,
}
