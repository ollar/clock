from app.events import EventsEmitter
from .wrappers import show_test_info


def test_func1():
    pass


def test_func2():
    pass


def test_func3():
    pass


@show_test_info
async def test_emitter_on():
    emitter = EventsEmitter()

    emitter.on('test', test_func1)
    emitter.on('test', test_func2)
    emitter.on('test', test_func3)

    assert len(emitter._events['test']) == 3


@show_test_info
async def test_emitter_off():
    emitter = EventsEmitter()

    emitter.on('test', test_func1)
    emitter.on('test', test_func2)
    emitter.on('test', test_func3)

    emitter.off('test', test_func3)

    assert len(emitter._events['test']) == 2
    assert set([test_func1]).issubset(emitter._events['test'])
    assert set([test_func2]).issubset(emitter._events['test'])
    assert not set([test_func3]).issubset(emitter._events['test'])



@show_test_info
async def test_emitter_emit():
    emitter = EventsEmitter()

    ii = 0

    def test_func1():
        nonlocal ii
        ii += 1


    def test_func2():
        nonlocal ii
        ii += 1


    def test_func3():
        nonlocal ii
        ii += 1


    emitter.on('test', test_func1)
    emitter.on('test', test_func2)
    emitter.on('test', test_func3)

    emitter.emit('test')

    assert ii == 3
