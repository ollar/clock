from app.button import Button
from app.stubs import machine
from app.events import event_emitter
from lib.functools import partial

from .wrappers import show_test_info

try:
    import uasyncio
except ImportError as e:
    import asyncio as uasyncio


@show_test_info
async def test_button_down():
    callback_called = False

    def event_callback(*args, **kwargs):
        nonlocal callback_called 
        callback_called = True

    event_emitter.on('button::test_down::down', event_callback)
    
    pin = machine.Pin(1)
    button = Button(pin=pin, button_name='test_down')

    listen_changes = uasyncio.create_task(button.listen_changes())

    assert callback_called == False

    pin.value(1)

    await uasyncio.sleep(0.2)

    assert callback_called == True

    listen_changes.cancel()
    event_emitter.off('button::test_down::down')


@show_test_info
async def test_button_up():
    callback_called = False

    def event_callback(*args, **kwargs):
        nonlocal callback_called 
        callback_called = True

    event_emitter.on('button::test_up::up', event_callback)
    
    pin = machine.Pin(1)
    button = Button(pin=pin, button_name='test_up')

    listen_changes = uasyncio.create_task(button.listen_changes())

    pin.value(1)

    await uasyncio.sleep(0.2)

    assert callback_called == False

    pin.value(0)

    await uasyncio.sleep(0.2)

    assert callback_called == True

    listen_changes.cancel()
    event_emitter.off('button::test_up::up')


@show_test_info
async def test_button_press():
    callback_called = False

    def event_callback():
        nonlocal callback_called
        callback_called = True

    event_emitter.on('button::test_press::press', event_callback)
    
    pin = machine.Pin(1)
    button = Button(pin=pin, button_name='test_press')

    listen_changes = uasyncio.create_task(button.listen_changes())

    assert callback_called == False
    
    pin.value(1)

    await uasyncio.sleep(1.1)

    pin.value(0)

    await uasyncio.sleep(0.2)

    assert callback_called == True

    listen_changes.cancel()
    event_emitter.off('button::test_press::press')


@show_test_info
async def test_button_long_press():
    callback_called = False

    def event_callback():
        nonlocal callback_called
        callback_called = True

    event_emitter.on('button::test_long_press::long_press', event_callback)
    
    pin = machine.Pin(1)
    button = Button(pin=pin, button_name='test_long_press')

    listen_changes = uasyncio.create_task(button.listen_changes())

    assert callback_called == False
    
    pin.value(1)

    await uasyncio.sleep(3.1)

    pin.value(0)

    await uasyncio.sleep(0.2)

    assert callback_called == True

    listen_changes.cancel()
    event_emitter.off('button::test_long_press::long_press')


@show_test_info
async def test_button_looong_press():
    callback_called = False

    def event_callback():
        nonlocal callback_called
        callback_called = True

    event_emitter.on('button::test_looong_press::looong_press', event_callback)
    
    pin = machine.Pin(1)
    button = Button(pin=pin, button_name='test_looong_press')

    listen_changes = uasyncio.create_task(button.listen_changes())

    assert callback_called == False
    
    pin.value(1)

    await uasyncio.sleep(5.1)

    pin.value(0)

    await uasyncio.sleep(0.2)

    assert callback_called == True

    listen_changes.cancel()
    event_emitter.off('button::test_looong_press::looong_press')


@show_test_info
async def test_button_calls_all_events_on_press():
    callbacks_called = []

    def event_callback(num, *args, **kwargs):
        nonlocal callbacks_called 
        callbacks_called.append(num)

    event_emitter.on('button::test_all_events::down', partial(event_callback, 1))
    event_emitter.on('button::test_all_events::press', partial(event_callback, 21))
    event_emitter.on('button::test_all_events::long_press', partial(event_callback, 22))
    event_emitter.on('button::test_all_events::looong_press', partial(event_callback, 23))
    event_emitter.on('button::test_all_events::up', partial(event_callback, 3))
    
    pin = machine.Pin(1)
    button = Button(pin=pin, button_name='test_all_events')

    listen_changes = uasyncio.create_task(button.listen_changes())

    # no events called
    assert callbacks_called == []
    
    pin.value(1)

    await uasyncio.sleep(5.1)

    pin.value(0)

    await uasyncio.sleep(0.2)

    assert callbacks_called == [1,21,22,23,3]

    listen_changes.cancel()
    event_emitter.off('button::test_all_events::down')
    event_emitter.off('button::test_all_events::press')
    event_emitter.off('button::test_all_events::long_press')
    event_emitter.off('button::test_all_events::looong_press')
    event_emitter.off('button::test_all_events::up')
