from app.controls import Controls
from app.stubs import machine
from app.events import event_emitter
from functools import partial
from .wrappers import show_test_info
try:
    import uasyncio
except ImportError as e:
    import asyncio as uasyncio


@show_test_info
async def test_control():
    pass