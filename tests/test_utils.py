from app.utils import array_to_matrix, prepare_matrix_for_device, matrix_to_array, multi_num_matrix_to_scalar,\
     num_matrix_to_pixel_matrix, multi_tuple_matrix_to_scalar, union_matrixes, get_transition_screen
from app.helpers import numbers, screen_matrix
from .wrappers import show_test_info


@show_test_info
async def test_get_transition_screen():
    screen1 = [(0,0,0), (0,0,0), (0,0,0)]
    screen2 = [(100,100,100), (100,100,100), (100,100,100)]
    expected_screen = [(50,50,50),(50,50,50),(50,50,50)]
    assert get_transition_screen(screen1, screen2) == expected_screen, 'transition screen schould be correct'


@show_test_info
async def test_array_to_matrix():
    test_array = [1,2,3,4,5,6,7,8,9]
    assert array_to_matrix(test_array, 3) == [[1,2,3],[4,5,6],[7,8,9]], 'array to matrix should create correct data'


@show_test_info
async def test_prepare_matrix_for_device():
    test_matrix = [[1,2,3],[4,5,6],[7,8,9]]
    expected_matrix = [[3,2,1],[4,5,6],[9,8,7]]
    assert list(prepare_matrix_for_device(test_matrix)) == expected_matrix, 'matrix %2 lines should be reversed'


@show_test_info
async def test_matrix_to_array():
    test_matrix = [[1,2,3],[4,5,6],[7,8,9]]
    expected_array = [1,2,3,4,5,6,7,8,9]
    assert list(matrix_to_array(test_matrix)) == expected_array, 'matrix convertation to array failed'


@show_test_info
async def test_array_to_matrix_to_array():
    test_array = [1,2,3,4,5,6,7,8,9]
    assert list(matrix_to_array(array_to_matrix(test_array, 3))) == test_array, 'matrix transformation not works in both sides'


@show_test_info
async def test_num_matrix_to_pixel_matrix():
    test_matrix = [[0,0,1], [1,1,1], [0,0,1]]
    expected_result = [
        [(0, 0, 0),(0, 0, 0),(1, 1, 1)],
        [(1, 1, 1),(1, 1, 1),(1, 1, 1)],
        [(0, 0, 0),(0, 0, 0),(1, 1, 1)]
    ]
    assert num_matrix_to_pixel_matrix(test_matrix) == expected_result, 'num matrix did not transform well to pixels matrix'


@show_test_info
async def test_multi_num_matrix_to_scalar():
    test_matrix = [[0,0,1], [1,1,1], [0,0,1]]
    scalar = 10
    expected_result = [[0,0,10], [10,10,10], [0,0,10]]
    assert multi_num_matrix_to_scalar(test_matrix, scalar) == expected_result, 'num matrix multi scalar did not match the expected result'


@show_test_info
async def test_multi_matrix_to_scalar():
    test_matrix = [
        [(0, 0, 0),(0, 0, 0),(1, 1, 1)],
        [(1, 1, 1),(1, 1, 1),(1, 1, 1)],
        [(0, 0, 0),(0, 0, 0),(1, 1, 1)],
    ]
    scalar = (10, 10, 10)
    expected_result = [
        [(0, 0, 0),(0, 0, 0),(10, 10, 10)],
        [(10, 10, 10),(10, 10, 10),(10, 10, 10)],
        [(0, 0, 0),(0, 0, 0),(10, 10, 10)]
    ]
    assert multi_tuple_matrix_to_scalar(test_matrix, scalar) == expected_result, 'matrix multi scalar did not match the expected result'


@show_test_info
async def tests_union_matrixes():
    test_matrix = [
        [(0, 0, 0),(0, 0, 0),(1, 1, 1)],
        [(1, 1, 1),(1, 1, 1),(1, 1, 1)],
        [(0, 0, 0),(0, 0, 0),(1, 1, 1)],
        [(0, 0, 0),(0, 0, 0),(0, 0, 0)],
    ]
    shape_matrix = [
        [(10, 10, 10),(10, 10, 10)],
        [(10, 10, 10),(10, 10, 10)]
    ]
    expected_result = [
        [(0, 0, 0),(10, 10, 10),(11, 11, 11)],
        [(1, 1, 1),(11, 11, 11),(11, 11, 11)],
        [(0, 0, 0),(0, 0, 0),(1, 1, 1)],
        [(0, 0, 0),(0, 0, 0),(0, 0, 0)],
    ]
    assert union_matrixes(test_matrix, shape_matrix, (1,0)) == expected_result, 'union matrixes not work'


@show_test_info
async def test_write_number_on_display():
    screen_matrix = [
        [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
        [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
        [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
        [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
        [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)]
    ]
    zero_matrix = numbers[0]
    expected_result = [
        [(1, 1, 1), (1, 1, 1), (1, 1, 1), (0, 0, 0), (0, 0, 0)],
        [(1, 1, 1), (0, 0, 0), (1, 1, 1), (0, 0, 0), (0, 0, 0)],
        [(1, 1, 1), (0, 0, 0), (1, 1, 1), (0, 0, 0), (0, 0, 0)],
        [(1, 1, 1), (0, 0, 0), (1, 1, 1), (0, 0, 0), (0, 0, 0)],
        [(1, 1, 1), (1, 1, 1), (1, 1, 1), (0, 0, 0), (0, 0, 0)],
    ]
    assert(union_matrixes(screen_matrix, zero_matrix, (0, 0))) == expected_result, 'write numbers not work'

    screen_matrix = [
        [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
        [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
        [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
        [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
        [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
        [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
        [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
        [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
        [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
        [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
    ]

    zero_matrix = numbers[0]
    two_matrix = numbers[2]

    expected_result = [
        [(1, 1, 1), (1, 1, 1), (1, 1, 1), (0, 0, 0), (1, 1, 1), (1, 1, 1), (1, 1, 1), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
        [(1, 1, 1), (0, 0, 0), (1, 1, 1), (0, 0, 0), (0, 0, 0), (0, 0, 0), (1, 1, 1), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
        [(1, 1, 1), (0, 0, 0), (1, 1, 1), (0, 0, 0), (1, 1, 1), (1, 1, 1), (1, 1, 1), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
        [(1, 1, 1), (0, 0, 0), (1, 1, 1), (0, 0, 0), (1, 1, 1), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
        [(1, 1, 1), (1, 1, 1), (1, 1, 1), (0, 0, 0), (1, 1, 1), (1, 1, 1), (1, 1, 1), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
        [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
        [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
        [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
        [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
        [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)],
    ]
    assert(
        union_matrixes(union_matrixes(screen_matrix, zero_matrix, (0, 0)), two_matrix, (4, 0))
    ) == expected_result, 'write double numbers not work'
