from .wrappers import show_test_info
from app.file import FileStorage
import os


@show_test_info
async def test_file_write():
    filename = 'test-write' 
    try:
        data = {'aaa': 1} 
        file_storage = FileStorage(filename)
        file_storage.write(data=data)

        assert file_storage.read() == data

        
        data = {'bbb': 2}
        file_storage.write(data)

        assert file_storage.read() == data

        data = {'dance': {'all': {'night': True}}}
        file_storage.write(data)

        assert file_storage.read() == data
        

    finally:
        try:
            os.remove(filename)
        except FileNotFoundError:
            pass


@show_test_info
async def test_file_read():
    filename = 'test-read' 
    try:
        file_storage = FileStorage(filename)
        data = file_storage.read()

        assert data == {}
    finally:
        try:
            os.remove('test-read')
        except FileNotFoundError:
            pass

