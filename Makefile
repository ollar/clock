clear:
	esptool.py erase_flash

install_firmware:
	esptool.py --baud 460800 write_flash 0x1000 ESP32_GENERIC-20241129-v1.24.1.bin

install_firmware_s3:
	esptool.py --port /dev/cu.usbmodem599D1094641 --baud 460800 write_flash 0 ESP32_GENERIC_S3-20241129-v1.24.1.bin

copy_files:
	mkdir res
	cp -r lib res/lib
	cp -r app res/app
	cp main.py res/main.py
	find res | grep __pycache__ | xargs rm -rf
	mpremote cp -r res/* :
	rm -rf res

mount_folder:
	mpremote mount .

run_tests:
# 	$(MAKE mount_folder) + run run_tests.py $@
	mpremote mount . + run run_tests.py

run:
	# mpremote run main.py
	mpremote mount . + run main.py

.PHONY: dev tests prod
