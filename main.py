try:
    import uasyncio
except ImportError:
    import asyncio as uasyncio
from app.main import App


async def main():
    app = App()
    await app.init_screen()


if __name__ == '__main__':
    uasyncio.run(main())
